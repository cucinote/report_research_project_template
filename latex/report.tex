
\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage{hyperref}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.3\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Instabilities and turbulence}
\author{Emilie Cucinotta}

% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}

% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\tableofcontents

\newpage

\section{Introduction}
The purpose of this report is to present two examples of turbulence and instabilities.
The first part is about the Lotka-Volterra prey-predator model, the associated equations and the solutions of the system. 
The second example evokes the separation of boundary and laminar layers, more precisly the importance of holes in golf balls.

\section{The prey-predator model}

\subsection{Context, social and scientific issues}
The aim of the prey-predator model, also well known as the Lotka-Volterra model is to describe the dynamics of biological systems in which a predator and its prey interact.
\\In 1926, Volterra published a book entitled "Mathematical Theory of the Struggle for Life" which followed his observation that after the Second World War (a period when fishing had greatly diminished) sharks and other predators were much more numerous than before the war and that after
\\
\newline  Mathematically speaking, this model is described with non-linear differential equations of the first order.
 
\subsection{Mathematical aspect}
\subsubsection{Equations}
\begin{itemize}
\item Explanation of the problem
\\ In this situation, we are looking at two different populations : the preys and the predators. The study consists in evaluate how the predators affect the preys and vice versa by taking into consideration that the preys are considered as the predators' food.
\item Setting up the problem into equations
\\ By making the hypothesis that the preys can unilimited eating and the predators can only eat preys, we can write the situation with this system:
\\
\begin{equation*}
 \begin{cases}
 X'=AX-BXY\ \\
 Y'=-CY+DXY\
 \end{cases}
\end{equation*}
\\ With X the preys, Y the predators and A,B,C,D are real and positive parameters.
\\ Let's assume that we know the number of preys and predators at t=0, we can already see that X'(0)<0 and Y'(0)>0	wich means that the predator population will increase. However, we can easily understand that it will not be valid on the long term. We will see and analyse graphics later in order to understand the correlation between de two species.

\subsubsection{Fixed points}
In furtherance of solving the system, the first step is to find the fixed points, points for wich the system is stationary :
$(X',Y')=(0,0)$ . Then we will have to linearize the system in order to find the eigenvalues and eigenmodes.

\item $(X,Y)=(0,0)$ :
\\ The first "obvious" fixed point is (0,0) wich correspond to the extinction of the two species. 

\item The second fixed point is $(X,Y)=(\frac{C}{D},\frac{A}{B})$ 
\\
\\\advice{ We can notice that the first fixed point is also knwon as the trivial solution and the second one as the non-trivial solution}
 

\subsubsection{Stability of the fixed points}
To study the stability of the fixed points, we have to linearise the equations around the fixed points.

\item (0,0) :
\\ If we linearize the system as :
\begin{equation*}
 \begin{cases}
 X=X_f + x\ \\
 Y=Y_f + x\
 \end{cases}
\end{equation*}
And in this case : $X_f = 0$ and $Y_f = 0$, we have :
\begin{equation*}
 \begin{cases}
 x'=Ax\ \\
 y'=-Cy\
 \end{cases}
\end{equation*}

The two equations can be rewritten as $V'=JV$ with $J$ the jacobian matrix of the system and $V=(x,y)$. So we have
\\
$V=\begin{pmatrix}
 x \\
 y
\end{pmatrix}$

$J=\begin{bmatrix}
A & 0 \\
0 & -C
\end{bmatrix}$

We notice that $J$ is already diagonal so the eigenvalues are $\sigma_1=A $ and $\sigma_2=-C$, and the eigenvectors are :
$V_1=\begin{pmatrix}
 1 \\
 0
\end{pmatrix}$ and 
$V_2=\begin{pmatrix}
 0 \\
 1
\end{pmatrix}$
\\The fixed point (0,0) is unstable because A and C are positive. $\sigma_1>0$ and $\sigma_2<0$ represent the saddle point.

\item $(\frac{C}{D},\frac{A}{B}) $:
\\ We linearise again the system and we obtain : 
\begin{equation*}
 \begin{cases}
 x'=\frac{-BCy}{D}\ \\
 y'=\frac{ADx}{B}\
 \end{cases}
\end{equation*}
\\ So the Jacobian matrix is :
$J=\begin{bmatrix}
0 & \frac{-BC}{D} \\
\frac{AD}{B} & 0
\end{bmatrix}$
\\ This time, the Jacobian matrix is non-diagonal so we have to solve :
\\ $det(J-\sigma_i Id)=0$
\\
$\Leftrightarrow
det \begin{pmatrix}
0-\sigma_i & \frac{-BC}{D} \\
\frac{AD}{B} & 0-\sigma_i
\end{pmatrix}$ 

So $\sigma_\pm=\pm i\sqrt{AC}$ and 

$V_\pm=\begin{pmatrix}
 \pm i\frac{B}{D}\sqrt{AC} \\
 1
\end{pmatrix}$
\\ The second fixed point $(\frac{C}{D},\frac{A}{B}) $ is stable. 




\subsection{Solutions with numerical simulations}
\subsubsection{Prey-predator through time}
In this subsection, we are going to look at the program that leads to numerical simulations and graphics. 
This program has been done by our teacher and needed several packages to use it such as Fluidsim installed via mamba. The code is available in the appendix. 
\\ 
\newline The code gives us a graphic~\ref{preydaprey time} that represents the amount of preys and predators through time and it highlights the phenomen of reverse wich is cyclic. As a matter of fact, we see at the begining that the predator's growth will stop and preys will increase.
\newline
\\ At $t\approx 1$, predators are at the maximum and preys are decreasing. Preys are going to increase again lately when predators will die of starvation.
\\Forutnately at $t=6$ the raise of preys will stop the fall of the number of predators.
\\ And the same observations append again and again, the functions X and Y are periodic. 
\newpage


\begin{figure}[!h] 
\centering
\includegraphics[scale=1]{../Pysimul/fig/fig_predaprey.png}
\caption{Evolution of the number of preys X and predators Y as functions of time}
\label{preydaprey time}
\end{figure}

It is a good way to  represent how much dependent the two species are. However this representation is linked to time, this could be great to express the correlation of the system without depending of time.



\subsubsection{Prey-predator}
Figure~\ref{trajectories} shows the relation between preys and predators without time. So we can rewrite the system as : 
\newline
\\
$ t \Rightarrow \begin{pmatrix}
 X(t) \\
 Y(t)
\end{pmatrix}$
 
The calcul of the system's solution give the trajectory of it, more precisly the vectory field wich is represented as lines in the graphic. Each line reflects the system's solution for one initial condition and so there's an infinity of solutions depending on the inital condition.
\newpage


\begin{figure}[!h]
\centering
\includegraphics[scale=1]{../Pysimul/fig/fig_isocontours.png} 
\caption{Trajectories for the prey(X) - predator (Y) system, to go through in an anti-clockwise counterclockwise. Infinity of solutions depending on initial condition. }
\label{trajectories}
\end{figure}

\advice{We can see that time is no longer here so we have a perfect correlation between preys and predators but it turns out that we loose a bit of information such as "how long preys are going to increase when predators are at their lowest?" .}

\end{itemize}

\subsection{Conclusion}
To conclude this part, we now understand of important depend the species are. Indeed, if one manages to disappear, the other will also irrevocably disappear. However they do act cyclically, they regulate themselves. 

\newpage


\section{Separation of boundary layer : golf balls}
\subsection{History}
In the old time, gold balls were made of leather pockets filled with wet goose feathers. People believed that smoothy they were, the farest they could go but they were completly wrong ! In 1890 Peter Guthrie Tait, passionated by golf understood and proved the importance of holes in golf balls.

\subsection{Equations and explanation}
To understant better what happend, let's focus first on the drag force on a flat plate of length L in a uniform flow. Let's suppose that the velocity varies along the solid wall, which occurs for example if the flow is divergent or convergent or if the profile of the wall is curved.
\\ So, the Bernouilli relation gives :
\begin{equation}
\frac{\partial P}{\partial x} = -\rho U(x) \frac{\partial U}{\partial x}
\end{equation}
This shows that if the velocity decreases going downstream, then the pressure increases along x. This downstream pressure gradient is also found in the boundary layer because the pressure hardly depends on the transverse coordinate y.
\\
\newline
According to the Navier-Stokes equations in the boundary layer, this increasing downstream pressure gradient will therefore tend to slow down the flow, while the viscous difusion from the outside into the boundary layer tends to accelerate it 
 as in the previous case.
So if the slowing down of U(x) is sufficiently important, the pressure gradient could be sufficient to cancel or reverse locally the direction of the flow in the boundary layer: it will occur a detachment of this one, which will increase the drag brutally. 
\newline 
\\ Turbulent boundary layers are more resistant to disbonding than laminar boundary layers in the presence of negative downstream velocity gradients. This is due to the fact that momentum transport by convection in the turbulent zone is more efficient than its diffusion by viscosity. The high velocities of the outer zone are therefore more easily transmitted to the wall in the turbulent case, which delays the reversal of the flow direction at the wall. We see on the top picture~\ref{golf} that in the laminar case, the boundary layer takes off very early.
In the lower situation, a transition to turbulence has been induced by placing an asperity upstream of the separation point. The turbulent boundary layer then takes off much further towards the edge and so the golf ball can go way further.

\begin{figure}[!h]  
\centering
\includegraphics[scale=1]{golf.jpg} 
\caption{Boundary layer separation on smooth sphere and sphere with cells}
\label{golf}
\end{figure}

\newpage

\subsection{Conclusion}
To conclude this part, the purpose of the dimples on golf balls is to lengthen the boundary layer / increase the stall length in order to limit friction as much as possible. As a matter of fact, the dimples are creating turbulence wich is more resistant to disbonding.

\newpage 

\section{Appendix}

\begin{figure}[!h] 
\centering
\includegraphics[scale=1]{preydaprey.jpg} 
\caption{Code for numerical simulations prey-predator through time model}
\label{fig_simple}
\end{figure}

\begin{figure}[!h] 
\centering
\includegraphics[scale=1]{prog_iso.jpg} 
\caption{Code for numerical simulations prey-predator model, isocontour of potential}
% \label{fig_simple}
\end{figure}

\newpage 
  

\end{document}
